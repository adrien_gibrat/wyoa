var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var fs = require("fs");

app.use(bodyParser.json()); // for parsing application/json

app.get('/a/:ida/pages', function (req, res) {
	var prefix = req.params.ida + "-";
	fs.readdir( __dirname, function (err, files) {
		var pages = files.filter(function(elt) {
			return elt.lastIndexOf(prefix, 0) === 0 &&
				elt.lastIndexOf(".page", elt.lenght-5) === elt.length-5; 
		}).map(function(elt) {
			return elt.slice(prefix.length, -5);
		});

		res.json({"pages": pages });
	});
})

app.post('/a/:ida/pages/:id/raw', function (req, res) {
	var raw = req.body.raw;
	var ida = req.params.ida;
	var id = req.params.id;
	fs.writeFile( __dirname + "/" + ida + "-" + id + ".page", raw, 'utf8', function (err, data) {
		var links = getLinks(raw);
		res.json({"raw": raw, "links": links });
	});
})

app.get('/a/:ida/pages/:id/raw', function (req, res) {
	var ida = req.params.ida;
	var id = req.params.id;
	fs.readFile( __dirname + "/" + ida + "-" + id + ".page", 'utf8', function (err, data) {
		var raw = "", links = [];
		if (data != undefined) {
			raw = data;
			links = getLinks(data);
		}
		res.json({"raw": raw, "links": links});
	});
})

app.delete('/a/:ida/pages/:id/raw', function (req, res) {
	var ida = req.params.ida;
	var id = req.params.id;
	fs.unlink( __dirname + "/" + ida + "-" + id + ".page", function() {
		res.status(204).send({});
	});
})

app.use(express.static(__dirname + '/assets'));

function getLinks(text) {
	var patt = /\[(.+?)\]/g;
	var m;
	var res = [ ];
	while(m = patt.exec(text)) {
		res.push(m[1]);
	}
	return res;
}

var server = app.listen(8081, function () {

   var host = server.address().address
   var port = server.address().port

   console.log("Example app listening at http://%s:%s", host, port)

})
